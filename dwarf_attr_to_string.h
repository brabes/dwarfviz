#pragma once

#include <QString>
#include <elfutils/libdw.h>

// formats an attribute into something meaningful, including decoding locexps
extern QString format_attr(Dwarf_Attribute *attr);
