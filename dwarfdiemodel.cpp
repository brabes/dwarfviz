
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <elfutils/libdw.h>

#include <stdexcept>

#include <QQueue>
#include <QVector>
#include <QSize>

#include "dwarfdiemodel.h"
#include "diemodelitem.h"
#include "dwarf_descs.h"

DwarfDieModel::DwarfDieModel(QObject *parent)
	: QAbstractItemModel(parent)
{
}

DwarfDieModel::DwarfDieModel(const QString& filename, QObject *parent)
	: QAbstractItemModel(parent)
{
	m_fd = open(filename.toLatin1(), O_RDONLY);
	if(m_fd == -1)
		throw std::runtime_error(std::string("Unable to open file:") + strerror(errno));
	m_dw = dwarf_begin(m_fd, DWARF_C_READ);
	if(m_dw == nullptr)
		throw std::runtime_error(std::string("Unable to read DWARF information: ") + dwarf_errmsg(dwarf_errno()));
	Dwarf_CU *cu = nullptr;
	Dwarf_Half ver;
	uint8_t type;
	Dwarf_Die cudie, subdie;

	// the root item is a 'nothing' item that contains the CUs
	// and conceptually represents the whole file
	m_rootItem = new DieModelItem(nullptr);
	while(dwarf_get_units(m_dw, cu, &cu, &ver, &type, &cudie, &subdie) == 0)
	{
		// depth-first tree traversal, we build our DieModelItem tree as we go

		// algorithm is:
		// q <- empty
		// q.enqueue(root)
		// while(not q.empty())
		//  e <- q.dequeue()
		//  visit(e)
		//  if(e.has_children())
		//	  foreach(child c of e)
		//		  q.enqueue(c)
		//	  endforeach
		//  endif
		// endwhile
		QQueue<DieModelItem*> queue;
		DieModelItem *cuitem = new DieModelItem(cudie, m_rootItem);
		m_rootItem->appendChild(cuitem);
		queue.enqueue(cuitem);
		while(!queue.empty())
		{
			DieModelItem *item = queue.dequeue();
			// queue children and add to this item
			Dwarf_Die kiddie;
			if(dwarf_child(item->die(), &kiddie) == 0)
			{
				DieModelItem *child = new DieModelItem(kiddie, item);
				item->appendChild(child);
				queue.enqueue(child);
				Dwarf_Die sibdie = kiddie;
				while(dwarf_siblingof(&sibdie, &sibdie) == 0)
				{
					child = new DieModelItem(sibdie, item);
					item->appendChild(child);
					queue.enqueue(child);
				}
			}
		}
	}
}

DwarfDieModel::~DwarfDieModel()
{
	delete m_rootItem;
	dwarf_end(m_dw);
	close(m_fd);
}

QVariant DwarfDieModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if(orientation == Qt::Orientation::Horizontal)
	{
		switch(role)
		{
		case Qt::DisplayRole:
			switch(section)
			{
			case 0: return QString("Name");
			case 1: return QString("Tag");
			}
			break;
		case Qt::SizeHintRole:
			if(section == 0) return QSize(500, 0);
			break;
		}

	}
	return QVariant::Invalid;
}

QModelIndex DwarfDieModel::index(int row, int column, const QModelIndex &parent) const
{
	if(!hasIndex(row, column, parent))
		return QModelIndex();

	DieModelItem *parentItem;
	if(parent.isValid())
		parentItem = static_cast<DieModelItem*>(parent.internalPointer());
	else
		parentItem = m_rootItem;

	DieModelItem *childItem = parentItem->child(row);
	if(childItem)
		return createIndex(row, column, childItem);

	return QModelIndex();
}

QModelIndex DwarfDieModel::parent(const QModelIndex &index) const
{
	if(!index.isValid())
		return QModelIndex();

	DieModelItem *item = static_cast<DieModelItem*>(index.internalPointer());
	DieModelItem *parent = item->parent();

	if(parent == nullptr || parent == m_rootItem)
		return QModelIndex();
	else
		return createIndex(parent->row(), 0, parent);
}

int DwarfDieModel::rowCount(const QModelIndex &parent) const
{
	DieModelItem *item;
	if(!parent.isValid())
		item = m_rootItem;
	else
		item = static_cast<DieModelItem*>(parent.internalPointer());
	return item->childCount();
}

int DwarfDieModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 2;
}

QVariant DwarfDieModel::data(const QModelIndex &index, int role) const
{
	if(!index.isValid())
		return QVariant();

	if(role != Qt::DisplayRole)
		return QVariant();

	DieModelItem *item = static_cast<DieModelItem*>(index.internalPointer());
	switch(index.column())
	{
	case 0:
	{
		const char *name = dwarf_diename(item->die());
		if(name == nullptr)
			name = "(no name)";
		return name;
	}
	case 1:
		return get_tag_desc(dwarf_tag(item->die()));
	}
	return QVariant();
}
