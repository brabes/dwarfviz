
#include "mainwindow.h"

#include <QApplication>

#include <QStyle>
#include <QDesktopWidget>
#include <QScreen>

int main(int argc, char *argv[])
{
	QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QApplication app(argc, argv);
	MainWindow win;
	QList<QScreen*> scrs = app.screens();
	QScreen *currentScreen = scrs[app.desktop()->screenNumber(&win)];
	win.setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, win.size(), currentScreen->availableGeometry()));
	win.show();
	return app.exec();
}
