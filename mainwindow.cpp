
#include "mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QApplication>
#include <QTimer>
#include <QTreeView>
#include <QListView>
#include <QBoxLayout>
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>
#include <QDockWidget>
#include <QGroupBox>

#include "dwarfdiemodel.h"
#include "dwarfattrmodel.h"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, m_model(nullptr)
	, m_attrModel(nullptr)
{
	setWindowTitle("DwarfViz debug information browser");

	// set up menus and toolbar
	createActions();

	QGroupBox *groupBox = new QGroupBox("Debug Information Entries", this);

	// the main widget, the tree of DIEs
	m_tree = new DieTreeView(this);
	if(!QObject::connect(m_tree, SIGNAL(currentchanged(const QModelIndex&)), this, SLOT(updateAttrWindow(const QModelIndex&))))
	{
		QMessageBox::critical(parent, "Error", "Cannot connect signal");
		QApplication::quit();
	}

	QBoxLayout *layout = new QBoxLayout(QBoxLayout::LeftToRight);
	layout->addWidget(m_tree);
	groupBox->setLayout(layout);

	setCentralWidget(groupBox);

	// the attributes dock widget
	QDockWidget *dock = new QDockWidget("Attributes", this);
	dock->setAllowedAreas(Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea);
	m_attrListView = new QTreeView(dock);
	dock->setWidget(m_attrListView);
	addDockWidget(Qt::BottomDockWidgetArea, dock);
}

MainWindow::~MainWindow()
{
}

void MainWindow::createActions()
{
	QMenu *fileMenu = menuBar()->addMenu(tr("&File"));

	QToolBar *fileToolBar = addToolBar(tr("File"));
	const QIcon openIcon = QIcon::fromTheme("document-open", this->style()->standardIcon(QStyle::SP_DialogOpenButton));
	QAction *openFileAct = new QAction(openIcon, "&Open file...", this);
	openFileAct->setShortcuts(QKeySequence::Open);
	openFileAct->setStatusTip("Open an ELF file");
	connect(openFileAct, &QAction::triggered, this, &MainWindow::openFile);
	fileToolBar->addAction(openFileAct);
	fileMenu->addAction(openFileAct);

	fileMenu->addSeparator();

	QAction *quitAct = fileMenu->addAction(tr("&Quit"), this, &QWidget::close);
	quitAct->setShortcuts(QKeySequence::Quit);
	quitAct->setStatusTip(tr("Quit the application"));

	menuBar()->addSeparator();
	QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));

	QAction *aboutAct = helpMenu->addAction(tr("&About"), this, &MainWindow::aboutApplication);
	aboutAct->setStatusTip(tr("Show the application's About box"));

	QAction *aboutQtAct = helpMenu->addAction(tr("About &Qt"), qApp, &QApplication::aboutQt);
	aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
}

void MainWindow::showEvent(QShowEvent *event)
{
	QWidget::showEvent(event);
	QTimer::singleShot(0, this, SLOT(openFile()));
}

void MainWindow::openFile()
{
	QString filePath = QFileDialog::getOpenFileName(this, "Choose an ELF executable");
	if(!filePath.isEmpty())
	{
		DwarfDieModel *newModel = nullptr;
		try
		{
			newModel = new DwarfDieModel(filePath);
			m_tree->setModel(newModel);
			delete m_model;
			m_model = newModel;
			m_tree->resizeColumnToContents(0);
			m_tree->resizeColumnToContents(1);
		}
		catch(std::runtime_error& e)
		{
			QMessageBox::critical(this, "Error opening file", e.what());
		}
	}
}

void MainWindow::updateAttrWindow(const QModelIndex &index)
{
	DieModelItem *item = static_cast<DieModelItem*>(index.internalPointer());
	Dwarf_Die *die = item->die();

	DwarfAttrModel *model = new DwarfAttrModel(die, m_attrListView);
	m_attrListView->setModel(model);
	delete m_attrModel;
	m_attrModel = model;
}

void MainWindow::aboutApplication()
{
	QMessageBox::information(this, "About", "DwarfViz version 0.0");
}
