#pragma once

#include <QAbstractTableModel>

#include <elfutils/libdw.h>

class DwarfAttrModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	explicit DwarfAttrModel(QObject *parent = nullptr);
	explicit DwarfAttrModel(Dwarf_Die *die, QObject *parent = nullptr);

	// Header:
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:

	static int attrCallback(Dwarf_Attribute*, void*); // used to populate attributes list

	QList<Dwarf_Attribute> m_attrs;
};
