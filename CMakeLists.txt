cmake_minimum_required(VERSION 3.5)

project(dwarfviz LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5 COMPONENTS Widgets REQUIRED)
find_package(PkgConfig REQUIRED)

pkg_check_modules(LIBELF libelf)
pkg_check_modules(LIBDW libdw)

add_executable(dwarfviz
  main.cpp
  mainwindow.cpp
  mainwindow.h
  dwarfdiemodel.cpp
  dwarfdiemodel.h
  dwarf_attr_to_string.cpp
  dwarf_attr_to_string.h
  dwarf_descs.cpp
  dwarf_descs.h
  diemodelitem.h
  dwarfattrmodel.cpp
  dwarfattrmodel.h
  dietreeview.h
)

target_link_libraries(dwarfviz PRIVATE Qt5::Widgets ${LIBELF_LIBRARIES} ${LIBDW_LIBRARIES})
