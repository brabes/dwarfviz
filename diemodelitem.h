#pragma once

#include <elfutils/libdw.h>
#include <QVector>

class DieModelItem
{
public:
	explicit DieModelItem(DieModelItem *parent)
		: m_parent(parent)
	{ }
	explicit DieModelItem(Dwarf_Die& die, DieModelItem *parent)
		: m_die(die)
		, m_parent(parent)
	{ }
	~DieModelItem() { qDeleteAll(m_children); }
	Dwarf_Die *die() { return &m_die; }
	DieModelItem *parent() { return m_parent; }
	int row() const
	{
		if(m_parent)
			return m_parent->m_children.indexOf(const_cast<DieModelItem*>(this));
		else
			return 0;
	}
	void appendChild(DieModelItem* item) { m_children.append(item); }
	DieModelItem *child(int row) { return m_children[row]; }
	int childCount() const { return m_children.count(); }
private:
	Dwarf_Die m_die; // the underlying die
	DieModelItem *m_parent; // parent or null
	QVector<DieModelItem*> m_children;
};
