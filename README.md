## DwarfViz

### Overview

This program loads an ELF file containing DWARF debug information and shows
a tree of the compilation units and Debug Information Entries (DIEs) using
libdwarf. It also shows the attribute of each DIE and can decode location
expressions.

![screenshot](screenshot.png)

