
#include "dwarfattrmodel.h"
#include "dwarf_descs.h"
#include "dwarf_attr_to_string.h"

DwarfAttrModel::DwarfAttrModel(QObject *parent)
	: QAbstractTableModel(parent)
{
}

int DwarfAttrModel::attrCallback(Dwarf_Attribute *attr, void *param)
{
	DwarfAttrModel *model = static_cast<DwarfAttrModel*>(param);
	model->m_attrs.append(*attr);
	return DWARF_CB_OK;
}

DwarfAttrModel::DwarfAttrModel(Dwarf_Die *die, QObject *parent)
	: QAbstractTableModel(parent)
{
	dwarf_getattrs(die, attrCallback, this, 0);
}

QVariant DwarfAttrModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if(orientation == Qt::Orientation::Horizontal)
	{
		switch(role)
		{
		case Qt::DisplayRole:
			switch(section)
			{
			case 0: return QString("Name");
			case 1: return QString("Form");
			case 2: return QString("Value");
			}
			break;
		}

	}
	return QVariant::Invalid;
}

int DwarfAttrModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return m_attrs.count();
}

int DwarfAttrModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 3;
}

QVariant DwarfAttrModel::data(const QModelIndex &index, int role) const
{
	if(!index.isValid())
		return QVariant();

	if(role != Qt::DisplayRole)
		return QVariant();

	if(index.row() > m_attrs.count())
		return QVariant();

	Dwarf_Attribute attr = m_attrs[index.row()];

	switch(index.column())
	{
	case 0:
		return get_attr_desc(dwarf_whatattr(&attr));
	case 1:
		return get_form_desc(dwarf_whatform(&attr));
	case 2:
		return format_attr(&attr);
	}
	return QVariant();
}
