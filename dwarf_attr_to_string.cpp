
#include <stdint.h>
#include <string.h>

#include <dwarf.h>
#include <elfutils/libdw.h>

#include <QString>

#include "dwarf_descs.h"

static QString format_locexp(const Dwarf_Op *ops, size_t nOps)
{
	QString ret;

	for(size_t i=0;i<nOps;i++)
	{
		uint8_t op = ops[i].atom;
		uint32_t arg1 = (uint32_t)ops[i].number;
		uint32_t arg2 = (uint32_t)ops[i].number2;

		if(DW_OP_reg0 <= op && op <= DW_OP_reg31)
			ret += QString::asprintf("DW_OP_reg%u", op - DW_OP_reg0);
		else if(DW_OP_lit0 <= op && op <= DW_OP_lit31)
			ret += QString::asprintf("DW_OP_lit%u", op - DW_OP_lit0);
		else if(DW_OP_breg0 <= op && op <= DW_OP_breg31)
			ret += QString::asprintf("DW_OP_breg%u : %+d", op - DW_OP_breg0, (int32_t)arg1);
		else if(DW_OP_const1u <= op && op <= DW_OP_const8s)
		{
			bool is_signed = (op & 1) != 0;
			int argslen = 1 << ((op - DW_OP_const1u) >> 1);
			ret += QString::asprintf("DW_OP_const%d%s : ", argslen, is_signed ? "s" : "u");
			if(is_signed)
				ret += QString::asprintf("%d", (int32_t)arg1);
			else
				ret += QString::asprintf("%u", arg1);
		}
		else switch(op)
		{
		// literal encodings
		case DW_OP_constu:
			ret += QString::asprintf("DW_OP_constu : %u", arg1);
			break;
		case DW_OP_consts:
			ret += QString::asprintf("DW_OP_consts : %d", (int32_t)arg1);
			break;
		case DW_OP_addr:
			ret += QString::asprintf("DW_OP_addr : 0x%08x", arg1);
			break;
		// register based addressing
		case DW_OP_fbreg:
			ret += QString::asprintf("DW_OP_fbreg : %+d", (int32_t)arg1);
			break;
		case DW_OP_bregx:
			ret += QString::asprintf("DW_OP_bregx : reg%u %+d", arg2, (int32_t)arg1);
			break;
		// stack operations
		case DW_OP_dup:
			ret += QString::asprintf("DW_OP_dup");
			break;
		case DW_OP_drop:
			ret += QString::asprintf("DW_OP_drop");
			break;
		case DW_OP_pick:
			ret += QString::asprintf("DW_OP_pick : %u", arg1);
			break;
		case DW_OP_over:
			ret += QString::asprintf("DW_OP_over");
			break;
		case DW_OP_swap:
			ret += QString::asprintf("DW_OP_swap");
			break;
		case DW_OP_rot:
			ret += QString::asprintf("DW_OP_rot");
			break;
		case DW_OP_deref:
			arg1 = 4;
			// *** fall through *** //
		case DW_OP_deref_size:
			ret += QString::asprintf("DW_OP_deref");
			if(op == DW_OP_deref_size)
				ret += QString::asprintf("_size : %u", arg1);
			break;
		case DW_OP_xderef:
			arg1 = 4;
			// *** fall through *** //
		case DW_OP_xderef_size:
			ret += QString::asprintf("DW_OP_xderef");
			if(op == DW_OP_xderef_size)
				ret += QString::asprintf("_size : %u", arg1);
			break;
		// arithmetic and logical ops
		case DW_OP_abs:
			ret += QString::asprintf("DW_OP_abs");
			break;
		case DW_OP_and:
			ret += QString::asprintf("DW_OP_and");
			break;
		case DW_OP_div:
			ret += QString::asprintf("DW_OP_div");
			break;
		case DW_OP_minus:
			ret += QString::asprintf("DW_OP_minus");
			break;
		case DW_OP_mod:
			ret += QString::asprintf("DW_OP_mod");
			break;
		case DW_OP_mul:
			ret += QString::asprintf("DW_OP_mul");
			break;
		case DW_OP_neg:
			ret += QString::asprintf("DW_OP_neg");
			break;
		case DW_OP_not:
			ret += QString::asprintf("DW_OP_not");
			break;
		case DW_OP_or:
			ret += QString::asprintf("DW_OP_or");
			break;
		case DW_OP_plus:
			ret += QString::asprintf("DW_OP_plus");
			break;
		case DW_OP_plus_uconst:
			ret += QString::asprintf("DW_OP_plus_uconst : %u", arg1);
			break;
		case DW_OP_shl:
			ret += QString::asprintf("DW_OP_shl");
			break;
		case DW_OP_shr:
			ret += QString::asprintf("DW_OP_shr");
			break;
		case DW_OP_shra:
			ret += QString::asprintf("DW_OP_shra");
			break;
		case DW_OP_xor:
			ret += QString::asprintf("DW_OP_xor");
			break;
		// control flow ops
		case DW_OP_le:
			ret += QString::asprintf("DW_OP_le");
			break;
		case DW_OP_ge:
			ret += QString::asprintf("DW_OP_ge");
			break;
		case DW_OP_eq:
			ret += QString::asprintf("DW_OP_eq");
			break;
		case DW_OP_lt:
			ret += QString::asprintf("DW_OP_lt");
			break;
		case DW_OP_gt:
			ret += QString::asprintf("DW_OP_gt");
			break;
		case DW_OP_ne:
			ret += QString::asprintf("DW_OP_ne");
			break;
		case DW_OP_bra:
		case DW_OP_skip:
			ret += QString::asprintf("DW_OP_%s : %+d ", op == DW_OP_bra ? "bra" : "skip", (int16_t)arg1);
			// offset is relative to the following instruction
			// i.e. DW_OP_skip 0 is a nop
			if( i+1 >= nOps )
				ret += QString::asprintf(" (not allowed as last instruction)");
			else
			{
				Dwarf_Word target = ops[i+1].offset + (int16_t)arg1;
				size_t idx=0;
				while( idx < nOps && ops[idx].offset != target)
					++idx;
				if(idx == nOps)
					ret += QString::asprintf(" (unable to find branch target 0x%08lx)", target);
				else
					ret += QString::asprintf(" (target index = %lu)", idx);
			}
			break;
		// special ops
		case DW_OP_piece:
			ret += QString::asprintf("DW_OP_piece : %u", arg1);
			break;
		case DW_OP_nop:
			ret += QString::asprintf("DW_OP_nop");
			break;
		case DW_OP_form_tls_address:
			ret += "DW_OP_form_tls_address";
			break;
		case DW_OP_call_frame_cfa:
			ret += "DW_OP_call_frame_cfa";
			break;
		default:
			ret += QString::asprintf("Unknown op 0x%02x", op);
		}
		ret += QString::asprintf("; ");
	}

	return ret;
}

QString format_attr(Dwarf_Attribute *attr)
{
	QString ret;

	Dwarf_Half attrnum, form;
	attrnum = dwarf_whatattr(attr);
	form = dwarf_whatform(attr);

	switch(form)
	{
	case DW_FORM_addr:
		{
		Dwarf_Addr addr;
		if(dwarf_formaddr(attr, &addr) == 0)
			ret += QString::asprintf("%#lx", addr);
		else goto error;
		}
		break;
	case DW_FORM_data1:
	case DW_FORM_data2:
	case DW_FORM_data4:
	case DW_FORM_data8:
	case DW_FORM_udata:
		{
		Dwarf_Word udata;
		int width;
		if(form == DW_FORM_data1)
			width = 2;
		else if(form == DW_FORM_data2)
			width = 4;
		else if(form == DW_FORM_data4)
			width = 8;
		else
			width = 16;
		if(dwarf_formudata(attr, &udata) == 0)
			ret += QString::asprintf("0x%0*lx", width, udata);
		else goto error;
		}
		break;
	case DW_FORM_sdata:
		{
		Dwarf_Sword sdata;
		if(dwarf_formsdata(attr, &sdata) == 0)
			ret += QString::asprintf("%ld", sdata);
		else goto error;
		}
		break;
	case DW_FORM_flag:
		{
		bool flag;
		if(dwarf_formflag(attr, &flag) == 0)
			ret += QString::asprintf(flag ? "true" : "false");
		else goto error;
		}
		break;
	case DW_FORM_flag_present:
		ret += "true (implicit)";
		break;
	case DW_FORM_string:
	case DW_FORM_strp:
		{
		const char *str;
		if( (str = dwarf_formstring(attr)) )
			ret += QString::asprintf("%s", str);
		/* don't need to free the string */
		else goto error;
		}
		break;
	case DW_FORM_block:
	case DW_FORM_block1:
	case DW_FORM_block2:
	case DW_FORM_block4:
	case DW_FORM_exprloc:
		{
		Dwarf_Block blk;
		if(dwarf_formblock(attr, &blk) == 0)
		{
			ret += QString::asprintf("%lu byte ", blk.length);
			if(form == DW_FORM_exprloc)
				ret += "expression:";
			else
				ret += "block:";
			uint8_t *u8p = (uint8_t*)blk.data;
			for(Dwarf_Word i=0;i<blk.length;i++)
				ret += QString::asprintf(" %02x", u8p[i]);
		}
		else goto error;
		}
		break;
	case DW_FORM_ref1:
	case DW_FORM_ref2:
	case DW_FORM_ref4:
	case DW_FORM_ref8:
	case DW_FORM_ref_udata:
		{
QT_WARNING_PUSH
QT_WARNING_DISABLE_GCC("-Wdeprecated-declarations")
QT_WARNING_DISABLE_CLANG("-Wdeprecated-declarations")
		Dwarf_Off refoffs;
		if(dwarf_formref(attr, &refoffs) == 0)
			ret += QString::asprintf("<%lx>", refoffs);
		else
			goto error;
QT_WARNING_POP
		}
		break;
	default:
		ret += QString::asprintf("unknown form data %u", form);
		break;
	}
	if(attrnum == DW_AT_location
		|| attrnum == DW_AT_frame_base
		|| attrnum == DW_AT_data_member_location
		|| attrnum == DW_AT_string_length
		|| attrnum == DW_AT_return_addr)
	{
		Dwarf_Op *ops = NULL;
		size_t exprlen = 0;
		int r = dwarf_getlocation(attr, &ops, &exprlen);
		if(r < 0)
			goto error;
		else if(exprlen)
		{
			ret += " -> ";
			ret += format_locexp(ops, exprlen);
		}
		else
			ret += " -> (empty expression)";
	}
	else if(attrnum == DW_AT_encoding)
	{
		Dwarf_Word udata;
		if(dwarf_formudata(attr, &udata) != 0)
			goto error;
		ret += QString::asprintf(" (%s)", get_attrenc_desc(udata));
	}
	return ret;
error:
	ret += QString::asprintf("ERROR: format_attr(): %s", dwarf_errmsg(dwarf_errno()));
	return ret;
}
