#pragma once

#include <QTreeView>

// This is purely so we can signal when the highlighted item changes
class DieTreeView : public QTreeView
{
	Q_OBJECT

public:
	explicit DieTreeView(QWidget *parent) : QTreeView(parent) { }

public slots:
	virtual void currentChanged(const QModelIndex& current, const QModelIndex& previous) override
	{
		QTreeView::currentChanged(current, previous);
		emit currentchanged(current);
	}

signals:
	void currentchanged(const QModelIndex& current);
};
