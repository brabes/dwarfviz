#pragma once

#include <QMainWindow>
#include "dietreeview.h"

class DwarfDieModel;
class DwarfAttrModel;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();

public slots:
	void openFile();
	void aboutApplication();
	void updateAttrWindow(const QModelIndex &current);

protected:
	void showEvent(QShowEvent *event) override;

private:
	void createActions();

	DieTreeView *m_tree;
	QTreeView *m_attrListView;
	DwarfDieModel *m_model;
	DwarfAttrModel *m_attrModel;
};
