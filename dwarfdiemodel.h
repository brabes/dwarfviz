#pragma once

#include <QAbstractItemModel>

#include "diemodelitem.h"

class DwarfDieModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	explicit DwarfDieModel(QObject *parent = nullptr);
	explicit DwarfDieModel(const QString& filename, QObject *parent = nullptr);
	~DwarfDieModel();
	// Header:
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	// Basic functionality:
	QModelIndex index(int row, int column,
					  const QModelIndex &parent = QModelIndex()) const override;
	QModelIndex parent(const QModelIndex &index) const override;

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
	DieModelItem *m_rootItem;
	int m_fd;
	Dwarf *m_dw;
};
