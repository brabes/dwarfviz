#pragma once

extern "C"
{
	extern const char *get_attr_desc(int);		// stringification of DW_AT_*
	extern const char *get_form_desc(int);		// stringification of DW_FORM_*
	extern const char *get_tag_desc(int);		// stringification of DW_TAG_*
	extern const char *get_attrenc_desc(int);	// stringification of DW_ATE_*
}
